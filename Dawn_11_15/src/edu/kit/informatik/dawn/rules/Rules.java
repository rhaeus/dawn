/**
 * 
 */
package edu.kit.informatik.dawn.rules;

import java.util.List;

import edu.kit.informatik.dawn.DawnGameInputException;
import edu.kit.informatik.dawn.StateMachine.GameState;
import edu.kit.informatik.dawn.board.Board;
import edu.kit.informatik.dawn.board.Position;
import edu.kit.informatik.dawn.board.Token;
import edu.kit.informatik.dawn.ui.Command;

/**
 * Interface for rules that should be applied to dawn game
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public interface Rules {
    /**
     * Places a token on the given board with respect to the rules.
     * 
     * @param board representation of a playing field on which to place the token
     * @param possibleTokens List of possible tokens to place on board. 
     * Token is chosen depending on its length and {@code start} and {@code end} Position (match)
     * @param start start position of token
     * @param end end position of token
     * @return placed token, if successful, null otherwise
     * @throws DawnGameInputException if {@code possibleTokens} and {@code start} and {@code end} don't match 
     * or are against the rules
     */
    Token place(Board board, List<Token> possibleTokens, Position start, Position end) throws DawnGameInputException;
    
    /**
     * Moves a token on the path given by {@code steps} with respect to the rules.
     * The position of the token to move is stored as first element in {@code steps}
     * 
     * @param board board representation of a playing field on which to move a token
     * @param steps path to move on (first element is start position)
     * @param maxSteps maximum number of steps allowed to use for a move
     * @throws DawnGameInputException if path breaks the rules or move not possible
     */
    void move(Board board, List<Position> steps, int maxSteps) throws DawnGameInputException;
    
    /**
     * Calculates the game result with respect to the rules
     * 
     * @param board board representation of a playing field to calculate the result
     * @return result of the game
     */
    int calcResult(Board board);
    
    /**
     * Checks if a given command {@code cmd} is allowed in a given {@code state} of the game with respect to the rules
     * 
     * @param state game state, for which to check if the command is allowed
     * @param cmd Command to check
     * @return true if command is allowed, false otherwise
     */
    boolean isCommandAllowed(GameState state, Command cmd);
}
