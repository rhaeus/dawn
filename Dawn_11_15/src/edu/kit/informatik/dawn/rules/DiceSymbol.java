/**
 * 
 */
package edu.kit.informatik.dawn.rules;

/**
 * Represents the dice used for dawn game
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public enum DiceSymbol {
    /**
     * Dice Symbol which represents value 2.
     */
    TWO("2", 2),
    
    /**
     * Dice Symbol which represents value 3.
     */
    THREE("3", 3),
    
    /**
     * Dice Symbol which represents value 4.
     */
    FOUR("4", 4),
    
    /**
     * Dice Symbol which represents value 5.
     */
    FIVE("5", 5),
    
    /**
     * Dice Symbol which represents value 7.
     */
    SIX("6", 6),
    
    /**
     * Dice Symbol which represents value 7 (DAWN).
     */
    DAWN("DAWN", 7);
    
    private final String name; //string representation
    private final int value; //dice value
    
    /*
     * Private constructor to initialize enum with name and value.
     */
    private DiceSymbol(String name, int value) {
        this.name = name;
        this.value = value;
    }
    
    /**
     * Gets the name of the enum
     * 
     * @return enum name
     */
    public String getName() {
        return this.name();
    }
    
    /**
     * Gets the value of the enum
     * 
     * @return enum value
     */
    public int getValue() {
        return this.value;
    }
    
    /**
     * Returns DiceSymbol object for a given name, if available
     * 
     * @param name name of DiceSymbol to get
     * @return DiceSymbol with name {@code name}
     */
    public static DiceSymbol getFromName(String name) {
        for (DiceSymbol ds : DiceSymbol.values()) {
            if (ds.name.equals(name)) {
                return ds;
            }
        }
        return null;
    }
    
    /**
     * Gets minimum value of all DiceSymbols
     * 
     * @return minimum value of all DiceSymbols
     */
    public static int getMinValue() {
        int min = Integer.MAX_VALUE;
        for (DiceSymbol ds : DiceSymbol.values()) {
            if (ds.value < min) {
                 min = ds.value;
            }
        }
        return min;
    }
    
    /**
     * Gets maximum value of all DiceSymbols
     * 
     * @return maximum value of all DiceSymbols
     */
    public static int getMaxValue() {
        int max = Integer.MIN_VALUE;
        for (DiceSymbol ds : DiceSymbol.values()) {
            if (ds.value > max) {
                 max = ds.value;
            }
        }
        return max;
    }
}
