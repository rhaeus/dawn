/**
 * 
 */
package edu.kit.informatik.dawn.rules;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import edu.kit.informatik.dawn.DawnGameInputException;
import edu.kit.informatik.dawn.StateMachine.GameState;
import edu.kit.informatik.dawn.board.Board;
import edu.kit.informatik.dawn.board.Position;
import edu.kit.informatik.dawn.board.Token;
import edu.kit.informatik.dawn.board.TokenType;
import edu.kit.informatik.dawn.result.BreadthFirstSearch;
import edu.kit.informatik.dawn.result.FreeConnectedCellCounter;
import edu.kit.informatik.dawn.ui.Command;
import edu.kit.informatik.dawn.ui.MessageStrings;

/**
 * Implementation of rules for the game Dawn 11/15
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public class RulesDawn1115 implements Rules {
    private FreeConnectedCellCounter resultCalculation;
    private List<Command> alwaysAllowedCmd;
    
    /**
     * Constructor to create a new RulesDawn1115 object
     */
    public RulesDawn1115() {
        //use breadth first search to count free connected cells on board for result
        this.resultCalculation = new BreadthFirstSearch();
        this.alwaysAllowedCmd = Arrays.asList(Command.STATE, Command.PRINT, Command.QUIT, Command.RESET);
    }
    
    @Override
    public Token place(Board board, List<Token> possibleTokens, Position start, Position end) 
            throws DawnGameInputException {
        Objects.requireNonNull(board);
        Objects.requireNonNull(possibleTokens);
        Objects.requireNonNull(start);
        Objects.requireNonNull(end);
        
        //start and end position must align either horizontally or vertically
        if (!(start.alignHorizontal(end) || start.alignVertical(end))) {
            throw new DawnGameInputException(MessageStrings.AlIGNMENT_ERROR.toString());
        }
        
        int requestedStoneSize = Math.max(start.getHorizontalSteps(end), start.getVerticalSteps(end));
        
        //space to occupy must match stone size
        Token toPlace = null;
        for (Token t : possibleTokens) {
            if (t.getLength() == requestedStoneSize) {
                toPlace = t;
                break;
            }
        }
        
        if (toPlace == null) {
            throw new DawnGameInputException(MessageStrings.SIZE_MISMATCH.toString());
        }
        
        //check position
        if (toPlace.getLength() == DiceSymbol.DAWN.getValue()) {
            //Dawn is allowed to reach over borders of board, but must overlap with at least 1 cell
            //-> either start position or end position (or both) must be within board borders
            if (!(board.checkPosition(start) || board.checkPosition(end))) {
                throw new DawnGameInputException(MessageStrings.PLACE_LOC_ERROR.toString());
            }
        } else {
            //start position and end position must be within board borders
            if (!(board.checkPosition(start) && board.checkPosition(end))) {
                throw new DawnGameInputException(MessageStrings.PLACE_LOC_ERROR.toString());
            }
        }
        
        //crop Positions to fit on board
        Position croppedStart = start.crop(0, board.getRowCount() - 1, 0, board.getColumnCount() - 1);
        Position croppedEnd = end.crop(0, board.getRowCount() - 1, 0, board.getColumnCount() - 1);   
        
        //try to place stone
        if (board.place(toPlace, croppedStart, croppedEnd)) {
            return toPlace; //return placed token
        } else {
            throw new DawnGameInputException(MessageStrings.PLACE_ALREADY_OCCUPIED.toString());   
        }
    }

    @Override
    public void move(Board board, List<Position> steps, int maxSteps) throws DawnGameInputException {
        Objects.requireNonNull(board);
        Objects.requireNonNull(steps);
        if (maxSteps < 0) {
            throw new IllegalArgumentException("maxSteps must be a positive value.");
        }
        
        //check if given parameters respect the rules
        if (!board.checkPositions(steps) || !validateMoveSteps(board, steps, maxSteps)) {
            throw new DawnGameInputException(MessageStrings.INVALID_MOVE.toString());
        }
        
        //check if move is possible according to the rules
        if (isMovePossible(board, steps)) {
            //move stone to new position
            assert board.getCell(steps.get(0)) != null;
            assert board.getCell(steps.get(steps.size() - 1)) != null;
            
            Token movingToken = board.getCell(steps.get(0)).removeToken();
            board.getCell(steps.get(steps.size() - 1)).occupy(movingToken);
        } else {
            throw new DawnGameInputException(MessageStrings.MOVE_OCCUPIED_FIELD.toString());
        }
    }
    
    /*
     * Checks if steps are valid according to the rules
     */
    private boolean validateMoveSteps(Board board, List<Position> steps, int maxSteps) {
        Objects.requireNonNull(board);
        Objects.requireNonNull(steps);

        assert maxSteps >= 0;
        assert board.checkPositions(steps);
        
        //check if number of steps fits maxSteps
        if (steps.size() - 1 > maxSteps) { //-1 because first element is start position
            return false;
        }
        
        //check if each step is exactly one elementary step horizontally or vertically
        for (int i = 0; i < steps.size() - 1; i++) {
            if (!isElementaryStepHV(steps.get(i), steps.get(i + 1))) { 
                //walking other than vertical/horizontal elementary step
                return false;
            }
        }
        
        return true;
    }
    
    /*
     * Checks if move represented by {@code steps} is possible according to the rules
     */
    private boolean isMovePossible(Board board, List<Position> steps) {
        Objects.requireNonNull(board);
        Objects.requireNonNull(steps);
        assert board.checkPositions(steps);
        assert board.getCell(steps.get(0)).getOccupyingToken() != null;
        
        //check if start position contains movable token
        if (!board.getCell(steps.get(0)).getOccupyingToken().isMovable()) {
            return false;
        }
        //check if some field is occupied and therefore cant't be accessed for moving
        //except first entry, because this is the starting field and therefore occupied by the moving stone
        for (int i = 1; i < steps.size(); i++) {
            if (board.getCell(steps.get(i)).isOccupied()) {
                if (!steps.get(0).equals(steps.get(i))) { //occupied is ok if current pos is start pos
                    return false;
                }
            }
        }
        return true;
    }
    
    /*
     * Checks if two given positions represent an elementary step according to the rules
     *  An elementary step is a step in horizontal or vertical direction with length 1
     */
    private boolean isElementaryStepHV(Position p1, Position p2) {
        Objects.requireNonNull(p1);
        Objects.requireNonNull(p2);
        
        int horizontal = p1.getHorizontalDistance(p2);
        int vertical = p1.getVerticalDistance(p2);
        
        if (horizontal != 0 && vertical != 0) { 
            //diagonal step is not allowed
            return false;
        }
        
        return Math.max(horizontal, vertical) == 1;
    }

    @Override
    public int calcResult(Board board) {
        Objects.requireNonNull(board);
        
        Position pos;
        //calc free accessible cells for Vesta
        pos = board.getLuminaryPosition(TokenType.VESTA);
        if (pos == null) {
            throw new IllegalStateException("game is in invalid state for result calculation.");
        }
        int fv = resultCalculation.count(board, pos);
        
        //calc free accessible cells for Ceres
        pos = board.getLuminaryPosition(TokenType.CERES);
        if (pos == null) {
            throw new IllegalStateException("game is in invalid state for result calculation.");
        }
        int fc = resultCalculation.count(board, pos);
        
        //calc game result with formula
        return Math.max(fv, fc) + Math.abs(fv - fc);
    }

    @Override
    public boolean isCommandAllowed(GameState state, Command cmd) {
        Objects.requireNonNull(state);
        Objects.requireNonNull(cmd);
        
        switch(state.getPhase()) {
            case END:
                return cmd.equals(Command.SHOW_RESULT) || alwaysAllowedCmd.contains(cmd);          
            case ONE:
            case TWO:
                switch(state.getAction()) {
                    case NA_PLACE:
                        return cmd.equals(Command.SET_VC) || alwaysAllowedCmd.contains(cmd);
                    case NA_ROLL:
                        return cmd.equals(Command.ROLL) || alwaysAllowedCmd.contains(cmd);
                    case MC_PLACE:
                        return cmd.equals(Command.PLACE) || alwaysAllowedCmd.contains(cmd);
                    case NA_MOVE:
                        return cmd.equals(Command.MOVE) || alwaysAllowedCmd.contains(cmd);
                    case NONE:
                        return alwaysAllowedCmd.contains(cmd);
                    default:
                        throw new IllegalStateException("undefined state.");
                }
            default:
                throw new IllegalStateException("undefined state.");      
        }
    }
}
