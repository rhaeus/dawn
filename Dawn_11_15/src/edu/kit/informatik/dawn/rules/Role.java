/**
 * 
 */
package edu.kit.informatik.dawn.rules;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import edu.kit.informatik.dawn.StateMachine.Phase;
import edu.kit.informatik.dawn.board.Token;
import edu.kit.informatik.dawn.board.TokenType;

/**
 * Represents a role of a player
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public enum Role {
    /**
     * Represents role Nature
     */
    NATURE() {
        @Override
        protected void init() {
            this.tokensMap = new HashMap<>();
            Map<Integer, Token> set;
            
            //player nature gets tokens Vesta and Ceres with length 1
            set = new HashMap<>();
            set.put(1, new Token(TokenType.VESTA, 1));
            tokensMap.put(Phase.ONE, set);
            
            set = new HashMap<>();
            set.put(1, new Token(TokenType.CERES, 1));
            tokensMap.put(Phase.TWO, set); 
        }

        @Override
        public List<Token> getPossibleTokens(Phase phase, int length) {
            Objects.requireNonNull(phase);
            
            assert length == 1;
            
            //retrieve token of requested phase 
            Map<Integer, Token> activeSet = getTokenSet(phase);
            Token token = activeSet.get(length);
            
            if (token != null) {
                assert !token.isUsed();
                return Arrays.asList(token);
            } else {
                return null;
            }  
        }
    },
    
    /**
     * Represents role Mission Control
     */
    MISSION_CONTROL() {
        @Override
        protected void init() {
            this.tokensMap = new HashMap<>();
            Map<Integer, Token> set;
            //player mission control gets tokens with length of each dawn symbol of dice
            for (Phase p : Phase.values()) {
                set = new HashMap<>();
                if (!p.equals(Phase.END)) { //only consider active phases                     
                    for (int length = DiceSymbol.getMinValue(); length <= DiceSymbol.getMaxValue(); length++) {
                        set.put(length, new Token(TokenType.SONDE, length));
                    }
                }
                this.tokensMap.put(p, set);
            }
        }

        @Override
        public List<Token> getPossibleTokens(Phase phase, int length) {
            Objects.requireNonNull(phase);
            if (length < 1) {
                throw new IllegalArgumentException("parameter length must be a positive number.");
            }
            
            List<Token> result = new ArrayList<>();
            
            Map<Integer, Token> currentSet = this.tokensMap.get(phase);
            Token requestedToken = currentSet.get(length);
            
            assert requestedToken != null;
            
            //if no such token return null
            if (requestedToken == null) {
                return null;
            }

            if (!requestedToken.isUsed()) {
                //if token with desired length is available
                return Arrays.asList(requestedToken);
            } else {
                //find next possible tokens to use           
                Token temp = null;
                
                //find next smaller
                for (int i = length - 1; i >= DiceSymbol.getMinValue(); i--) {
                    temp = currentSet.get(i);
                    if (temp != null && !temp.isUsed()) {
                        result.add(temp);
                        break;
                    }
                }
                
               //find next greater
                for (int i = length + 1; i <= DiceSymbol.getMaxValue(); i++) {
                    temp = currentSet.get(i);
                    if (temp != null && !temp.isUsed()) {
                        result.add(temp);
                        break;
                    }
                }

                if (result.isEmpty()) {
                    return null; //token not available
                } else {
                    return result; //list of available token(s)
                }    
            }   
        }
    };
    
    /**
     * Stores a player's tokens
     */
    protected Map<Phase, Map<Integer, Token>> tokensMap;
    
    /*
     * Constructor of Role enum
     * Initializes enum
     */
    private Role() {
        init();
    }
    
    /**
     * Initializes the enum
     */
    protected abstract void init();
    
    /**
     * Returns a list of possible tokens depending on game phase and required token length.
     * If token with length {@code length} is not available anymore, tokens with next smaller
     * and/or next greater length are returned.
     * 
     * @param phase phase to get token of
     * @param length desired length of token
     * @return List of possible tokens to use next, null if no token available
     */
    public abstract List<Token> getPossibleTokens(Phase phase, int length);
    
    /**
     * Gets the players token set of the given game phase
     * 
     * @param phase phase for which to get the players token set
     * @return token set of {@code phase}
     */
    public Map<Integer, Token> getTokenSet(Phase phase) {
        Objects.requireNonNull(phase);
        assert tokensMap.get(phase) != null;
        
        return tokensMap.get(phase);
    }
    
    /**
     * Resets the enum state
     */
    public static void resetAll() {
        for (Role r : Role.values()) {
            r.init();
        }
    }
}
