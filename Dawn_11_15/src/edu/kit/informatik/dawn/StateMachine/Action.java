/**
 * 
 */
package edu.kit.informatik.dawn.StateMachine;

/**
 * Represents actions in a game phase of dawn game
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public enum Action {
    /**
     * Represents action Nature places token
     */
    NA_PLACE,
    
    /**
     * Represents action Nature rolls
     */
    NA_ROLL,
    
    /**
     * Represents action Mission control places token
     */
    MC_PLACE,
    
    /**
     * Represents action Nature moves token
     */
    NA_MOVE,
    
    /**
     * Represents a state, where no action can be performed. End state.
     */
    NONE
}
