/**
 * 
 */
package edu.kit.informatik.dawn.StateMachine;

/**
 * Represents a phase of the game
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public enum Phase {
    /**
     * Represents phase 1 of the game
     */
    ONE {
        @Override
        public Phase next() {
            return TWO;
        }
    },
    
    /**
     * Represents phase 2 of the game
     */
    TWO {
        @Override
        public Phase next() {
            return END;
        }
    },
    
    /**
     * Represents the end phase of the game
     */
    END {
        @Override
        public Phase next() {
            return END;
        }
    };
    
    /**
     * Number of Rounds per active phase (not end phase)
     */
    public static final int NUMBER_OF_ROUNDS = 6;
    
    /**
     * Returns the next phase
     * 
     * @return next phase
     */
    public abstract Phase next();
}
