/**
 * 
 */
package edu.kit.informatik.dawn.StateMachine;

/**
 * Represents the game state of dawn game
 * Controls the game order
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public class GameState {
    private Phase phase; //current game phase
    private Action action; //current action to be performed
    private int roundCounter = 0; //counts the rounds in a phase
    
    /**
     * Constructor to create a new GameState object
     */
    public GameState() {
        //init for start of game
        this.phase = Phase.ONE;
        this.action = Action.NA_PLACE;
        this.roundCounter = 0;
    }
    
    /**
     * Gets phase of game state object
     * 
     * @return phase
     */
    public Phase getPhase() {
        return this.phase;
    }
    
    /**
     * Gets Action of game state object
     * 
     * @return action
     */
    public Action getAction() {
        return this.action;
    }
    
    /**
     * Transitions the game state object into the next state
     */
    public void next() {
        switch(this.action) {
            case NA_PLACE:
                this.action = Action.NA_ROLL;
                //a new round starts if nature rolls
                this.roundCounter++;
                break;
            case NA_ROLL:
                this.action = Action.MC_PLACE;
                break;
            case MC_PLACE:
                this.action = Action.NA_MOVE;
                break;
            case NA_MOVE:
                //if all rounds of current phase are played, game enters next phase
                //Nature places next token
                if (this.roundCounter >= Phase.NUMBER_OF_ROUNDS) {
                    this.phase = this.phase.next();
                    if (this.phase.equals(Phase.END)) {
                        //if all active phases are played, game is in end state
                        //no actions performed in this state
                        this.action = Action.NONE;
                    } else {
                        this.action = Action.NA_PLACE;
                        //new phase -> rounds start over
                        this.roundCounter = 0;
                    }
                } else {
                    this.action = Action.NA_ROLL;
                    //a new round starts if nature rolls
                    this.roundCounter++;
                } 
                break;
            case NONE:
                //do nothing, end state
                break;
            default:
                throw new IllegalStateException("Invalid Game State.");
        }
    }
}
