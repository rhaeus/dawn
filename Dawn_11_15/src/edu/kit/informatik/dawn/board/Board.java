/**
 * 
 */
package edu.kit.informatik.dawn.board;

import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;


/**
 * Represents a board, consisting out of cells.
 * Can be used as playing field
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public class Board {
    private Cell[][] board; //playing field consisting of cells
    private int rows; //number of rows of this board
    private int columns; //number of columns of this board
    
    /**
     * Constructor to create a new board object with {@code rows} rows and {@columns} columns
     * 
     * @param rows number of rows for the board
     * @param columns number of columns for the board
     */
    public Board(int rows, int columns) {
        if (rows < 1 || columns < 1) {
            throw new IllegalArgumentException("rows and columns must be at least 1.");
        }
        
        this.rows = rows;
        this.columns = columns;
        //init board with cells
        this.board = new Cell[this.rows][this.columns];
        for (int row = 0; row < this.rows; row++) {
            for (int col = 0; col < this.columns; col++) {
                board[row][col] = new Cell(new Position(row, col));
            }
        }
    }
    
    /**
     * Gets the number of rows of this board
     * 
     * @return number of rows
     */
    public int getRowCount() {
        return this.rows;
    }
    
    /**
     * Gets the number of columns of this board
     * 
     * @return number of columns
     */
    public int getColumnCount() {
        return this.columns;
    }
    
    /**
     * Gets cell at given position {@code pos}
     * 
     * @param pos position on board of cell to get
     * @return cell at position {@code pos}
     */
    public Cell getCell(Position pos) {
        Objects.requireNonNull(pos);
        
        //check if given pos is within board borders
        if (checkPosition(pos)) {
            return board[pos.getRow()][pos.getCol()];
        } else {
            return null;
        }
    }
    
    /**
     * Checks if given position {@code pos} is within board borders
     * 
     * @param pos Position to check
     * @return true if pos is within borders, false if not
     */
    public boolean checkPosition(Position pos) {
        Objects.requireNonNull(pos);
        
        return pos.getRow() >= 0 && pos.getRow() < this.rows 
                && pos.getCol() >= 0 && pos.getCol() < this.columns;
    }
    
    /**
     * Checks if given positions {@code pos} are all within board borders
     * 
     * @param pos Positions to check
     * @return true if positions are all within borders, false if not
     */
    public boolean checkPositions(List<Position> pos) {
        Objects.requireNonNull(pos);
        
        for (Position p : pos) {
            if (!checkPosition(p)) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Checks if cell at given position {@code pos} on board is occupied by a token
     * 
     * @param pos position of cell to check
     * @return true if occupied, false if not
     */
    public boolean isPositionOccupied(Position pos) {
        Objects.requireNonNull(pos);
        assert checkPosition(pos);
        return getCell(pos).isOccupied();
    }
    
    /**
     * Places given Token {@code token} at position specified by {@code start} and {@code end}
     * 
     * @param token token to place
     * @param start start position
     * @param end end position
     * @return true, if placing possible, false if not
     */
    public boolean place(Token token, Position start, Position end)  {
        Objects.requireNonNull(token);
        Objects.requireNonNull(start);
        Objects.requireNonNull(end);
        
        if (!checkPosition(start) || !checkPosition(end)) {
            throw new IllegalArgumentException("start or end out of board bounds.");
        }
        
        assert checkPosition(start) && checkPosition(end);
        
        //check if token fits in given space
        if (!tokenFits(start, end)) {
            return false;
        }
        
        if (start.alignHorizontal(end)) {
            //place stone horizontally
            //start > end allowed
            int lower = Math.min(start.getCol(), end.getCol());
            int upper = Math.max(start.getCol(), end.getCol());
            for (int i = lower; i <= upper; i++) {
                getCell(new Position(start.getRow(), i)).occupy(token);
            }
            //mark token as used after placing
            token.use();
        } else if (start.alignVertical(end)) {
            //place stone vertically
            //start > end allowed
            int lower = Math.min(start.getRow(), end.getRow());
            int upper = Math.max(start.getRow(), end.getRow());
            
            for (int i = lower; i <= upper; i++) {
                getCell(new Position(i, start.getCol())).occupy(token);
            }
            //mark token as used after placing
            token.use();
        } else {
            throw new IllegalArgumentException("start and end must align either horizontally or vertically");
        }
        
        return true;
    }
    
    /*
     * Checks if there is enough space on the board to place a token at the given position
     */
    private boolean tokenFits(Position start, Position end) {
        Objects.requireNonNull(start);
        Objects.requireNonNull(end);
        
        if (!checkPosition(start) || !checkPosition(end)) {
            throw new IllegalArgumentException("Argument start or end out of board bounds");
        }
        
        //check if token fits
        boolean fits = true;
        if (start.alignHorizontal(end)) {
            //place stone horizontally
            int lower = Math.min(start.getCol(), end.getCol());
            int upper = Math.max(start.getCol(), end.getCol());
            //check if a cell is already occupied in requested space
            for (int i = lower; i <= upper; i++) {
                if (isPositionOccupied(new Position(start.getRow(), i))) {
                    fits = false;
                }
            }
        } else {
            //place stone vertically
            int lower = Math.min(start.getRow(), end.getRow());
            int upper = Math.max(start.getRow(), end.getRow());
            //check if a cell is already occupied in requested space
            for (int i = lower; i <= upper; i++) {
                if (isPositionOccupied(new Position(i, start.getCol()))) {
                    fits = false;
                }
            }
        }
        return fits;
    }
    
    /**
     * Gets position of a given {@code luminary}
     * 
     * @param luminary luminary to get position of
     * @return Position of {@code luminary}
     */
    public Position getLuminaryPosition(TokenType luminary) {
        Objects.requireNonNull(luminary);
        
        if (!luminary.isLuminary()) {
            throw new IllegalArgumentException("given token is not a luminary.");
        }
        
        Token temp;
        //Traverse board and look for luminary
        for (int row = 0; row < this.rows; row++) {
          for (int col = 0;  col < this.columns; col++) {
              temp = this.board[row][col].getOccupyingToken();
              if (temp != null) {
                  if (temp.getType().equals(luminary))
                      //luminary position found
                      return new Position(row, col);
                  }
              }
          }
        return null; //luminary not found
    }
    
    /**
     * Checks if given {@code luminary} is stuck, means not horizontally or vertically movable
     * 
     * @param luminary luminary to check on
     * @return true, if {@code luminary} is stuck, false if not
     */
    public boolean isLuminaryStuckHV(TokenType luminary) {
        Objects.requireNonNull(luminary);
        
        //get luminary position and store all orthogonal positions
        Position lumPos = getLuminaryPosition(luminary);
        List<Position> orthogonalPos = lumPos.getOrthogonalPositions();
        
        boolean stuck = true;
        
        for (Position p : orthogonalPos) {
            if (checkPosition(p)) { //position is within board borders
                stuck &= getCell(p).isOccupied();
            } //if not, token can't move in this direction -> stuck remains true
        }
        return stuck;
    }
    
    @Override
    public String toString() {
        StringJoiner sj = new StringJoiner(System.lineSeparator());
        StringBuilder sb = new StringBuilder();
        
        for (int row = 0; row < this.rows; row++) {
            sb.setLength(0);
            for (int col = 0; col < this.columns; col++) {
                sb.append(this.board[row][col].toString());
            }
            sj.add(sb.toString());
        }
        return sj.toString();
    }
}
