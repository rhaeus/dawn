/**
 * 
 */
package edu.kit.informatik.dawn.board;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a position with row and column
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public class Position {
    private int row;
    private int col;
    
    /**
     * Constructor to create a new position object
     * 
     * @param row row of the position
     * @param col column of the position
     */
    public Position(int row, int col) {
        this.row = row;
        this.col = col;
    }
    
    /**
     * Gets the row of the position
     * 
     * @return row of the position
     */
    public int getRow() {
        return this.row;
    }
    
    /**
     * Gets the column of the position
     * 
     * @return column of the position
     */
    public int getCol() {
        return this.col;
    }
    
    /**
     * Gets number of positions covered horizontally with this 
     * and {@code other} position as end points (included)
     * 
     * @param other other position
     * @return number of positions covered horizontally
     */
    public int getHorizontalSteps(Position other) {
        return Math.abs(this.col - other.col) + 1;
    }
    
    /**
     * Gets number of positions covered vertically with this 
     * and {@code other} position as end points (included)
     * 
     * @param other other position
     * @return number of positions covered vertically
     */
    public int getVerticalSteps(Position other) {
        return Math.abs(this.row - other.row) + 1;
    }
    
    /**
     * Gets horizontal distance between this position and {@code other} position
     * 
     * @param other other position
     * @return horizontal distance
     */
    public int getHorizontalDistance(Position other) {
        return Math.abs(this.col - other.col);
    }
    
    /**
     * Gets vertical distance between this position and {@code other} position
     * 
     * @param other other position
     * @return vertical distance
     */
    public int getVerticalDistance(Position other) {
        return Math.abs(this.row - other.row);
    }
    
    /**
     * Checks if this position and the {@code other} position are horizontally aligned
     * 
     * @param other other position
     * @return true if horizontally aligned, false if not
     */
    public boolean alignHorizontal(Position other) {
        return getVerticalDistance(other) == 0;
    }
    
    /**
     * Checks if this position and the {@code other} position are vertically aligned
     * 
     * @param other other position
     * @return true if vertically aligned, false if not
     */
    public boolean alignVertical(Position other) {
        return getHorizontalDistance(other) == 0;
    }
    
    /**
     * Crops this Position to {@code minRow}, {@code maxRow}, {@code minCol} and {@code maxCol}
     * and returns as new object
     * 
     * @param minRow minimum row of new object
     * @param maxRow maximum row of new object
     * @param minCol minimum column of new object
     * @param maxCol maximum column of new object
     * @return cropped position
     */
    public Position crop(int minRow, int maxRow, int minCol, int maxCol) {
        int row = Math.min(maxRow, Math.max(minRow, this.row));
        int col = Math.min(maxCol, Math.max(minCol, this.col));
        return new Position(row, col);
    }
    
    /**
     * Gets orthogonal positions of this position
     *
     * @return List of positions: one position above, below, to the right and to the left of this position
     */
    public List<Position> getOrthogonalPositions() {
        List<Position> neighbors = new ArrayList<>();
        neighbors.add(new Position(this.row - 1, this.col));
        neighbors.add(new Position(this.row + 1, this.col));
        neighbors.add(new Position(this.row, this.col - 1));
        neighbors.add(new Position(this.row, this.col + 1));
        return neighbors;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + col;
        result = prime * result + row;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Position other = (Position) obj;
        if (col != other.col)
            return false;
        if (row != other.row)
            return false;
        return true;
    }
    
}
