/**
 * 
 */
package edu.kit.informatik.dawn.board;

import edu.kit.informatik.dawn.result.BFSearchable;

/**
 * Represents a cell of a playing field.
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public class Cell implements BFSearchable {
    private Token occupyingToken = null; //token on this cell
    private boolean visited = false; //for breadth-first-search
    private Position pos; //position in surrounding board, can be null
    
    /**
     * Constructor to create a new cell object.
     * 
     * @param pos Position of this cell in surrounding playing field
     */
    public Cell(Position pos) {
        this.pos = pos;
    }
    
    /**
     * Gets position of this cell in surrounding playing field
     * 
     * @return Position, can be null if not supplied
     */
    public Position getPosition() {
        return this.pos;
    }
    
    /**
     * Occupies this cell with Token {@code token}
     * 
     * @param token Token to occupy with
     */
    public void occupy(Token token) {
        assert !isOccupied();
        
        this.occupyingToken = token;
        
    }
    
    /**
     * Removes occupying token from this cell
     * 
     * @return removed token
     */
    public Token removeToken() {
        assert isOccupied();
        Token removedToken = this.occupyingToken;
        this.occupyingToken = null;
        return removedToken;
    }
    
    /**
     * Gets the occupying token
     * 
     * @return occupying token
     */
    public Token getOccupyingToken() {
        return this.occupyingToken;
    }
    
    /**
     * Checks if this cell is occupied by a token
     * 
     * @return true if occupied, false if not
     */
    public boolean isOccupied() {
        return this.occupyingToken != null;
    }
    
    @Override
    public String toString() {
        if (!isOccupied()) {
            return "-";
        } else {
            return occupyingToken.toString();
        }
    }

    @Override
    public void visit() {
        assert !this.visited;
        this.visited = true;
    }

    @Override
    public boolean isVisited() {
        return this.visited;
    }

    @Override
    public void resetVisit() {
        this.visited = false;
    }
}
