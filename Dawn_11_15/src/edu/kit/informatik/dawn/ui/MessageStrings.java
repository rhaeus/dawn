/**
 * 
 */
package edu.kit.informatik.dawn.ui;

/**
 * Contains strings with messages to display to the user
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public enum MessageStrings {
    /**
     * Message for user if ok.
     */
    OK("OK"),
    
    /**
     * Message for user if given command is not valid.
     */
    INVALID_CMD("Not a valid command."),
    
    /**
     * Message for user if given coordinates are not horizontally or vertically aligned 
     * and therefore not valid for a move.
     */
    AlIGNMENT_ERROR("Coordinates are not horizontally or vertically aligned."),
    
    /**
     * Message for user if given place position doesn't match the size of the token to place.
     */
    SIZE_MISMATCH("Coordinates and token size don't match."),
    
    /**
     * Message for user if the given placing position for a token is invalid.
     */
    PLACE_LOC_ERROR("Invalid placing position."),
    
    /**
     * Message for user if there is not enough space to place the token at a given position.
     */
    PLACE_ALREADY_OCCUPIED("Token doesn't fit in given space."),
    
    /**
     * Message for user if the entered command is not possible in the current game state.
     */
    CMD_NOT_ALLOWED("Requested command is not possible in current game state."),
    
    /**
     * Message for user if the given move steps are not valid.
     */
    INVALID_MOVE("Invalid path."),
    
    /**
     * Message for user if given move is not possible because occupied fields on path.
     */
    MOVE_OCCUPIED_FIELD("Move not possible because the way is shut.");
    
    private final String message;
    
    /*
     * Private constructor to initialize enum object with it's message string
     */
    private MessageStrings(String message) {
        this.message = message;
    }
    
    @Override
    public String toString() {
        return message;
    }
}
