/**
 * 
 */
package edu.kit.informatik.dawn.ui;

/**
 * Holds Regex patterns for input validation for Dawn 11/15
 * 
 * NOTE TO CORRECTOR
 * This class produces in praktomat an "Variable access definition in wrong order."- Error
 * which should not happen. The problem is, that the public static final fields use some helper
 * private static final fields which are only needed to construct these public constants and are 
 * therefore declared private. Praktomat demands: first all public, then all private which would lead
 * to a compiler error because the public fields reference the private fields.
 * Check with allCheckStyleRules.xml results in no error and due to the documentation of Checkstyle
 * such references should be ignored for the declaration order check. 
 * (http://checkstyle.sourceforge.net/config_coding.html#DeclarationOrder)
 * I asked about this problem and different behaviour (Praktomat vs allChecksytyleRules) in the forum
 * and I was told this will be taken into account for the correction and I will not loose any points for this.
 * I also got a personal e-mail from Programmieren-Team which says I wont loose points for that.
 * To avoid this problem of course I could have declared everything public but in my opinion the helper fields
 * should be private.
 * 
 * This is the link to my forum question:
 * https://ilias.studium.kit.edu/ilias.php?ref_id=851721&cmdClass=ilobjforumgui&thr_pk=119393&
 * cmd=viewThread&cmdNode=gu:4&baseClass=ilRepositoryGUI
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public final class RegexPatterns {
    /**
     * Regex for show-result command.
     * Command expects no parameters.
     */
    public static final String REGEX_SHOW_RESULT = "show-result";
    
    /**
     * Regex for reset command.
     * Command expects no parameters.
     */
    public static final String REGEX_RESET = "reset";
    
    /**
     * Regex for quit command.
     * Command expects no parameters.
     */
    public static final String REGEX_QUIT = "quit";
    
    /**
     * Regex for print command. 
     * Command expects no parameters.
     */
    public static final String REGEX_PRINT = "print"; 
    
    /*
     * Regex for dice symbol. Number 2 - 6 or DAWN
     */
    private static final String REGEX_SYMBOL = "([2-6]|DAWN)";
    
    /**
     * Regex for roll command.
     * Command expects a dice symbol as parameter.
     */
    public static final String REGEX_ROLL = "roll " + REGEX_SYMBOL;
    
    /*
     * Regex for extended row coordinate of Dawn 11/15. Numbers -6 - 16, because DAWN token can reach over board borders
     */
    private static final String REGEX_EX_ROW_COORDINATE = "(-[1-6]|[0-9]|1[0-6])"; 
    
    /**
     * Regex for extended column coordinate of Dawn 11/15. 
     * Numbers -6 - 20, because DAWN token can reach over board borders
     */
    private static final String REGEX_EX_COL_COORDINATE = "(-[1-6]|[0-9]|1[0-9]|20)"; 
    
    /*
     * Regex for a extended coordinate of Dawn 11/15. 
     * Extended row coordinate and extended column coordinate separated by a semicolon in non capturing group
     */
    private static final String REGEX_EX_COORDINATE = "(?:" + REGEX_EX_ROW_COORDINATE + ";" 
                                + REGEX_EX_COL_COORDINATE + ")";
    
    /**
     * Regex for place command.
     * Command expects two coordinates separated by a colon.
     */
    public static final String REGEX_PLACE = "place " + REGEX_EX_COORDINATE + ":" + REGEX_EX_COORDINATE;
    
    /*
     * Regex for a row coordinate of Dawn 11/15. Numbers 0-10
     */
    private static final String REGEX_ROW_COORDINATE = "([0-9]|10)"; 
    
    /*
     * Regex for a column coordinate of Dawn 11/15. Numbers 0-14
     */
    private static final String REGEX_COL_COORDINATE = "([0-9]|1[0-4])"; 
    
    /*
     * Regex for a coordinate of Dawn 11/15. 
     * Row coordinate and column coordinate separated by a semicolon in non capturing group
     */
    private static final String REGEX_COORDINATE = "(?:" + REGEX_ROW_COORDINATE + ";" + REGEX_COL_COORDINATE + ")"; 
    
    /*
     * Regex for an additional optional coordinate. 
     * Separated with a colon at the beginning and in a non capturing group.
     */
    private static final String REGEX_ADDITIONAL_OPTIONAL_COOR = "(?::" + REGEX_COORDINATE + ")?";
    
    /**
     * Regex pattern for move command.
     * Command expects a number of coordinates separated by a colon.
     * Not implemented as repeating capturing group to be able to access each group with matcher.group(index).
     */
    public static final String REGEX_MOVE = "move " + REGEX_COORDINATE 
                                                + REGEX_ADDITIONAL_OPTIONAL_COOR 
                                                + REGEX_ADDITIONAL_OPTIONAL_COOR
                                                + REGEX_ADDITIONAL_OPTIONAL_COOR
                                                + REGEX_ADDITIONAL_OPTIONAL_COOR
                                                + REGEX_ADDITIONAL_OPTIONAL_COOR
                                                + REGEX_ADDITIONAL_OPTIONAL_COOR; 
    
    /**
     * Regex for state command.
     * Command expects one coordinate as parameter
     */
    public static final String REGEX_STATE = "state " + REGEX_COORDINATE;
    
    /**
     * Regex for set-vc command. 
     * Command expects one coordinate as parameter.
     */
    public static final String REGEX_SET_VC = "set-vc " + REGEX_COORDINATE;    
}
