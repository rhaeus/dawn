/**
 * 
 */
package edu.kit.informatik.dawn.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.dawn.DawnGameInputException;
import edu.kit.informatik.dawn.board.Game;
import edu.kit.informatik.dawn.board.Position;
import edu.kit.informatik.dawn.rules.DiceSymbol;

/**
 * Contains the available commands for the game
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public enum Command {
    /**
     * Represents the state command. 
     * Prints the state of a given playing field position to the user.
     */
    STATE(RegexPatterns.REGEX_STATE) {
        @Override
        public void execute(Game game) throws DawnGameInputException {
            //get coordinates from command
            //stored in group 1 and 2 of regex pattern
            int row = Integer.parseInt(matcher.group(1));
            int col = Integer.parseInt(matcher.group(2));
            Terminal.printLine(game.getCellState(new Position(row, col)));
        }
    },
    
    /**
     * Represents the print command.
     * Prints the entire playing field to the user.
     */
    PRINT(RegexPatterns.REGEX_PRINT) {
        @Override
        public void execute(Game game) throws DawnGameInputException {
            Terminal.printLine(game.printBoard());
        }
    },
    
    /**
     * Represents the set-vc command.
     * Allows the user to place Vesta or Ceres on the playing field.
     */
    SET_VC(RegexPatterns.REGEX_SET_VC) {
        @Override
        public void execute(Game game) throws DawnGameInputException {
            //get coordinates from command
            //stored in group 1 and 2 of regex pattern
            int row = Integer.parseInt(matcher.group(1));
            int col = Integer.parseInt(matcher.group(2));
            //Try to place token, if not possible InputException is thrown
            game.setVc(new Position(row, col));
            //placing was successful
            Terminal.printLine(MessageStrings.OK.toString());          
        }
    },
    
    /**
     * Represents the roll command.
     * Allows the user to roll the dice with Symbols 2,3,4,5,6,DAWN
     */
    ROLL(RegexPatterns.REGEX_ROLL) {
        @Override
        public void execute(Game game) throws DawnGameInputException {
            //match input to diceSymbol
            DiceSymbol ds = DiceSymbol.getFromName(matcher.group(1));
            if (ds == null) {
                Terminal.printError(MessageStrings.INVALID_CMD.toString());
            }
            
            //roll given number
            game.roll(ds);
            Terminal.printLine(MessageStrings.OK);
        }
    },
    
    /**
     * Represents the place command.
     * Allows the user to place a token on the playing field.
     */
    PLACE(RegexPatterns.REGEX_PLACE) {
        @Override
        public void execute(Game game) throws DawnGameInputException {
            int row1 = Integer.parseInt(matcher.group(1));
            int col1 = Integer.parseInt(matcher.group(2));
            int row2 = Integer.parseInt(matcher.group(3));
            int col2 = Integer.parseInt(matcher.group(4));
            
            game.place(new Position(row1, col1), new Position(row2, col2));
            Terminal.printLine(MessageStrings.OK.toString());
        }
    },
    
    /**
     * Represents the move command.
     * Allows the user to move a token on the playing field.
     */
    MOVE(RegexPatterns.REGEX_MOVE) {
        @Override
        public void execute(Game game) throws DawnGameInputException {
            int row;
            int col;
            List<Position> steps = new ArrayList<>();
            //collect steps
            //each coordinate is stored as a separate group in regex pattern
            for (int i = 1; i <= matcher.groupCount(); i += 2) {
                if (matcher.group(i) == null) {
                    break; //no more steps
                }
                row = Integer.parseInt(matcher.group(i));
                col = Integer.parseInt(matcher.group(i + 1));
                steps.add(new Position(row, col)); 
            }
            
            //try to move, if not possible, input exception is thrown
            game.move(steps);
            //move was successfull
            Terminal.printLine(MessageStrings.OK.toString());
        }
    },
    
    /**
     * Represents the show-result command.
     * Allows the user to print the game result at the end of the game.
     */
    SHOW_RESULT(RegexPatterns.REGEX_SHOW_RESULT) {
        @Override
        public void execute(Game game) throws DawnGameInputException {
            Terminal.printLine(game.getResult());
        }
    },
    
    /**
     * Represents the reset command.
     * Allows the user to reset the game and start over.
     */
    RESET(RegexPatterns.REGEX_RESET) {
        @Override
        public void execute(Game game) throws DawnGameInputException {
            game.reset();
            Terminal.printLine(MessageStrings.OK.toString());
        }
    },
    
    /**
     * Represents the quit command.
     * Allows the user to exit the game.
     */
    QUIT(RegexPatterns.REGEX_QUIT) {
        @Override
        public void execute(Game game) {
            quit();
        }
    };
    
    /**
     * Matcher object for regex. Contains matched input to use for further evaluation, e.g. regex groups.
     */
    protected Matcher matcher;
    private final Pattern pattern;
    private boolean quit = false;
    
    /*
     * Constructor for Command enum. Stores pattern for regex matching.
     */
    private Command(String pattern) {
        this.pattern = Pattern.compile(pattern);
    }
    
    
    /**
     * Checks if given command is quit command
     * 
     * @return true if command is quit command
     */
    public boolean isQuit() {
        return this.quit;
    }
    
    /**
     * Exits the program gracefully.
     */
    protected void quit() {
        quit = true;
    }
    
    /**
     * perform work of command
     * @param game game object, to perform action on
     * 
     * @throws DawnGameInputException if command is not allowed in current game state
     * or if parameters are not valid according to the rules
     */
    public abstract void execute(Game game) throws DawnGameInputException;

    /**
     * Tries to match input to a command and starts executing
     * 
     * @param input input to match on command
     * @param game game object on which to operate
     * @return matched command, if successful
     * @throws DawnGameInputException if input couldn't be matched to a command
     */
    public static Command matchAndExecute(String input, Game game) throws DawnGameInputException {
        for (Command command : Command.values()) {
            command.matcher = command.pattern.matcher(input);
            //if command matches input, perform work and return command object to caller
            if (command.matcher.matches()) {
                command.execute(game);
                return command;
            }
        }

        //no command could be matched to input
        throw new DawnGameInputException(MessageStrings.INVALID_CMD.toString());
    }
}
