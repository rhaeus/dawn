/**
 * 
 */
package edu.kit.informatik.dawn.result;

import edu.kit.informatik.dawn.board.Board;
import edu.kit.informatik.dawn.board.Position;

/**
 * Interface to count free connected cells surrounding a given cell
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public interface FreeConnectedCellCounter {
    /**
     * Counts free connected cells surrounding the position {@code root}
     * 
     * @param board board representation to count on
     * @param root starting point for counting
     * @return number of free connected cells surrounding {@code root}
     */
    int count(Board board, Position root);
}
