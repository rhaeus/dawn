package edu.kit.informatik.dawn;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.dawn.board.Game;
import edu.kit.informatik.dawn.ui.Command;

public class TestExampleInteraction {
    private Game game;
    
    @Before
    public void setUp() {
        Terminal.testMode = true;
        game = new Game();
    }
    
    @After
    public void tearDown() {
        Terminal.testMode = false;
    }
    
    @Test
    public void test() {
        Terminal.writeInputBuffer("set-vc -5;2");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
         
        Terminal.writeInputBuffer("set-vc 5;2");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("state 5;2");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
        
        Terminal.writeInputBuffer("roll DAWN");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("reset");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("set-vc 5;2");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll 5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 6;2:6;6");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 4;2:3;2");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll 5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 2;0:2;5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
             
        Terminal.writeInputBuffer("move 4;2:3;2");
        Loop.loop(game);
        String test = Terminal.readOutputBuffer();
        assertTrue(test.equals("OK"));
        
        Terminal.writeInputBuffer("roll DAWN");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 3;1:9;1");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 4;2");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll 3");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 5;3:5;5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 5;2:4;2:5;2");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll 2");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 0;0:0;1");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 4;2:5;2");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll 3");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 2;6:5;6");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 4;2:3;2:4;2");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("print");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("++-------------" + System.lineSeparator() 
                                                     + "---------------" + System.lineSeparator() 
                                                     + "+++++++--------" + System.lineSeparator() 
                                                     + "-+----+--------" + System.lineSeparator() 
                                                     + "-+V---+--------" + System.lineSeparator() 
                                                     + "-+-++++--------" + System.lineSeparator() 
                                                     + "-++++++--------" + System.lineSeparator() 
                                                     + "-+-------------" + System.lineSeparator() 
                                                     + "-+-------------" + System.lineSeparator() 
                                                     + "-+-------------" + System.lineSeparator()
                                                     + "---------------"));
        
        Terminal.writeInputBuffer("set-vc 3;5");
        Loop.loop(game);
        test = Terminal.readOutputBuffer();
        assertTrue(test.equals("OK"));
        
        Terminal.writeInputBuffer("roll DAWN");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 0;14:0;20");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 4;5:4;4:4;3:4;2:3;2:3;3:3;4");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("move 4;5:4;4:4;3:3;3:3;2:3;3:3;4");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll DAWN");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 10;0:10;5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 4;4:4;3:3;3:3;4:3;5:4;5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll 2");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 1;13:1;14");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 3;5:3;4");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll 5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 2;10:2;14");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 4;4:4;5:3;5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll 5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 3;11:3;14");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 4;5:3;5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll 3");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 4;12:4;10");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 3;4:3;5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("show-result");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("8"));
        
        Terminal.writeInputBuffer("print");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals(  "++------------+" + System.lineSeparator() 
                                                      + "-------------++" + System.lineSeparator() 
                                                      + "+++++++---+++++" + System.lineSeparator() 
                                                      + "-+---C+----++++" + System.lineSeparator() 
                                                      + "-+V---+---+++--" + System.lineSeparator() 
                                                      + "-+-++++--------" + System.lineSeparator() 
                                                      + "-++++++--------" + System.lineSeparator() 
                                                      + "-+-------------" + System.lineSeparator() 
                                                      + "-+-------------" + System.lineSeparator() 
                                                      + "-+-------------" + System.lineSeparator() 
                                                      + "++++++---------"));
        
        Terminal.writeInputBuffer("reset");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("set-vc 0;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("print");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals( "V--------------" + System.lineSeparator()
                                                      + "---------------" + System.lineSeparator() 
                                                      + "---------------" + System.lineSeparator() 
                                                      + "---------------" + System.lineSeparator() 
                                                      + "---------------" + System.lineSeparator() 
                                                      + "---------------" + System.lineSeparator() 
                                                      + "---------------" + System.lineSeparator() 
                                                      + "---------------" + System.lineSeparator() 
                                                      + "---------------" + System.lineSeparator() 
                                                      + "---------------" + System.lineSeparator() 
                                                      + "---------------"));

    }

}
