package edu.kit.informatik.dawn;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
   TestBFS.class,
   TestChooseToken2SameRolls.class,
   TestChooseToken6SameRolls.class,
   TestExampleInteraction.class,
   TestExampleMove.class,
   TestExamplePlace.class,
   TestInputFormat.class,
   TestMove.class,
   TestPlaceNotPossible.class
})

public class AllTestsSuite {
    
}
