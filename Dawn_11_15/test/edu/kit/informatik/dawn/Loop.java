package edu.kit.informatik.dawn;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.dawn.board.Game;
import edu.kit.informatik.dawn.ui.Command;

public class Loop {

    public static void loop(Game game){
        try {
            //try to match user input to a command
            Command.matchAndExecute(Terminal.readLine(), game);
        } catch (DawnGameInputException e) {
            Terminal.printError(e.getMessage()); //print error messages to the user
        }
    }
}
