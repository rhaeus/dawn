package edu.kit.informatik.dawn;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.dawn.board.Game;

public class TestExampleMove {
    Game game;
    
    @Before
    public void setUp() throws Exception {
        game = new Game();
        Terminal.testMode = true;
        Terminal.printToConsoleInTestMode = false;
    }

    @After
    public void tearDown() throws Exception {
        Terminal.testMode = false;
    }

    @Test
    public void test1() {
        //Vesta steht auf 0;0 und darf max 5 Schritte gehen
       Terminal.writeInputBuffer("set-vc 0;0");
       Loop.loop(game);
       assertTrue(Terminal.readOutputBuffer().equals("OK"));
       
       Terminal.writeInputBuffer("roll 5");
       Loop.loop(game);
       assertTrue(Terminal.readOutputBuffer().equals("OK"));
       
       Terminal.writeInputBuffer("place 10;1:10;5");
       Loop.loop(game);
       assertTrue(Terminal.readOutputBuffer().equals("OK"));
       
       Terminal.writeInputBuffer("move 0;1:0;2:0;3:0;4:0;5");
       Loop.loop(game);
       assertTrue(Terminal.readOutputBuffer().equals("OK"));
    }
    
    @Test
    public void test2() {
        //Vesta steht auf 0;1 und darf max 4 Schritte gehen
       Terminal.writeInputBuffer("set-vc 0;1");
       Loop.loop(game);
       assertTrue(Terminal.readOutputBuffer().equals("OK"));
       
       Terminal.writeInputBuffer("roll 4");
       Loop.loop(game);
       assertTrue(Terminal.readOutputBuffer().equals("OK"));
       
       Terminal.writeInputBuffer("place 10;1:10;4");
       Loop.loop(game);
       assertTrue(Terminal.readOutputBuffer().equals("OK"));
       
       Terminal.writeInputBuffer("move 0;1:0;2:0;3");
       Loop.loop(game);
       assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
    }
    
    @Test
    public void test3() {
        //Vesta steht auf 0;1 und darf max 4 Schritte gehen
       Terminal.writeInputBuffer("set-vc 0;1");
       Loop.loop(game);
       assertTrue(Terminal.readOutputBuffer().equals("OK"));
       
       Terminal.writeInputBuffer("roll 4");
       Loop.loop(game);
       assertTrue(Terminal.readOutputBuffer().equals("OK"));
       
       Terminal.writeInputBuffer("place 10;1:10;4");
       Loop.loop(game);
       assertTrue(Terminal.readOutputBuffer().equals("OK"));
       
       Terminal.writeInputBuffer("move 0;0:0;1:0;2:0;3");
       Loop.loop(game);
       assertTrue(Terminal.readOutputBuffer().equals("OK"));
    }
    
    @Test
    public void test4() {
        //Vesta steht auf 0;1 und darf max 7 Schritte gehen
       Terminal.writeInputBuffer("set-vc 0;1");
       Loop.loop(game);
       assertTrue(Terminal.readOutputBuffer().equals("OK"));
       
       Terminal.writeInputBuffer("roll DAWN");
       Loop.loop(game);
       assertTrue(Terminal.readOutputBuffer().equals("OK"));
       
       Terminal.writeInputBuffer("place 10;1:10;7");
       Loop.loop(game);
       assertTrue(Terminal.readOutputBuffer().equals("OK"));
       
       Terminal.writeInputBuffer("move 0;0");
       Loop.loop(game);
       assertTrue(Terminal.readOutputBuffer().equals("OK"));
    }
    
    @Test
    public void test5() {
        //own
        //Vesta steht auf 0;0 und darf max 5 Schritte gehen
       Terminal.writeInputBuffer("set-vc 0;0");
       Loop.loop(game);
       assertTrue(Terminal.readOutputBuffer().equals("OK"));
       
       Terminal.writeInputBuffer("roll 5");
       Loop.loop(game);
       assertTrue(Terminal.readOutputBuffer().equals("OK"));
       
       Terminal.writeInputBuffer("place 10;1:10;5");
       Loop.loop(game);
       assertTrue(Terminal.readOutputBuffer().equals("OK"));
       
       Terminal.writeInputBuffer("move 0;1:0;2:0;3:0;4:0;5:0;6");
       Loop.loop(game);
       assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
    }

}
