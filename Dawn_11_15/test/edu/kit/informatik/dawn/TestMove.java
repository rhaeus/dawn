package edu.kit.informatik.dawn;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.dawn.board.Game;

public class TestMove {
    Game game;
    
    @Before
    public void setUp() throws Exception {
        game = new Game();
        Terminal.testMode = true;
        Terminal.printToConsoleInTestMode = false;
    }

    @After
    public void tearDown() throws Exception {
    }

    private void prepareForMove(int roll) {
        //places like
        //V-++++...
        //------...
        //.........
        
        String sRoll;
        if (roll == 7) {
            sRoll = "DAWN";
        } else {
            sRoll = Integer.toString(roll);
        }
        
        Terminal.writeInputBuffer("reset");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("set-vc 0;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll " + sRoll);
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 0;2:0;" + (roll + 1));
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));     
    }
    
    @Test
    public void testMaxStep2() {
        //roll2----------------------------------------------------------------------
        prepareForMove(2);
        
        //move 0 fail
        Terminal.writeInputBuffer("move 0;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForMove(2);
        
        //move 1 ok
        Terminal.writeInputBuffer("move 1;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        prepareForMove(2);
        
        //move 2
        Terminal.writeInputBuffer("move 1;0:2;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        prepareForMove(2);
        
        //move 3 
        Terminal.writeInputBuffer("move 1;0:2;0:3;0");
        Loop.loop(game);
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForMove(2);
        
      //move 4 
        Terminal.writeInputBuffer("move 1;0:2;0:3;0:4;0");
        Loop.loop(game);
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForMove(2);
        
      //move 5 
        Terminal.writeInputBuffer("move 1;0:2;0:3;0:4;0:5;0");
        Loop.loop(game);
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForMove(2);
        
      //move 6 
        Terminal.writeInputBuffer("move 1;0:2;0:3;0:4;0:5;0:6;0");
        Loop.loop(game);
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForMove(2);
        
      //move 7
        Terminal.writeInputBuffer("move 1;0:2;0:3;0:4;0:5;0:6;0:7;0");
        Loop.loop(game);
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForMove(2);
        
      //move 8
        Terminal.writeInputBuffer("move 1;0:2;0:3;0:4;0:5;0:6;0:7;0:8;0");
        Loop.loop(game);
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
    }
    
    
    @Test
    public void testMaxStep3() {
        //roll3----------------------------------------------------------------------
        prepareForMove(3);
        
        //move 0 fail
        Terminal.writeInputBuffer("move 0;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForMove(3);
        
        //move 1 ok
        Terminal.writeInputBuffer("move 1;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        prepareForMove(3);
        
        //move 2
        Terminal.writeInputBuffer("move 1;0:2;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        prepareForMove(3);
        
        //move 3 
        Terminal.writeInputBuffer("move 1;0:2;0:3;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForMove(3);
        
      //move 4 
        Terminal.writeInputBuffer("move 1;0:2;0:3;0:4;0");
        Loop.loop(game);
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForMove(3);
        
      //move 5 
        Terminal.writeInputBuffer("move 1;0:2;0:3;0:4;0:5;0");
        Loop.loop(game);
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForMove(3);
        
      //move 6 
        Terminal.writeInputBuffer("move 1;0:2;0:3;0:4;0:5;0:6;0");
        Loop.loop(game);
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForMove(3);
        
      //move 7
        Terminal.writeInputBuffer("move 1;0:2;0:3;0:4;0:5;0:6;0:7;0");
        Loop.loop(game);
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForMove(3);
        
      //move 8
        Terminal.writeInputBuffer("move 1;0:2;0:3;0:4;0:5;0:6;0:7;0:8;0");
        Loop.loop(game);
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
    }
    
    
    @Test
    public void testMaxStep4() {
        //roll2----------------------------------------------------------------------
        prepareForMove(4);
        
        //move 0 fail
        Terminal.writeInputBuffer("move 0;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForMove(4);
        
        //move 1 ok
        Terminal.writeInputBuffer("move 1;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        prepareForMove(4);
        
        //move 2
        Terminal.writeInputBuffer("move 1;0:2;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        prepareForMove(4);
        
        //move 3 
        Terminal.writeInputBuffer("move 1;0:2;0:3;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForMove(4);
        
      //move 4 
        Terminal.writeInputBuffer("move 1;0:2;0:3;0:4;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForMove(4);
        
      //move 5 
        Terminal.writeInputBuffer("move 1;0:2;0:3;0:4;0:5;0");
        Loop.loop(game);
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForMove(4);
        
      //move 6 
        Terminal.writeInputBuffer("move 1;0:2;0:3;0:4;0:5;0:6;0");
        Loop.loop(game);
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForMove(4);
        
      //move 7
        Terminal.writeInputBuffer("move 1;0:2;0:3;0:4;0:5;0:6;0:7;0");
        Loop.loop(game);
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForMove(4);
        
      //move 8
        Terminal.writeInputBuffer("move 1;0:2;0:3;0:4;0:5;0:6;0:7;0:8;0");
        Loop.loop(game);
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
    }
    
    
    @Test
    public void testMaxStep5() {
        //roll2----------------------------------------------------------------------
        prepareForMove(5);
        
        //move 0 fail
        Terminal.writeInputBuffer("move 0;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForMove(5);
        
        //move 1 ok
        Terminal.writeInputBuffer("move 1;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        prepareForMove(5);
        
        //move 2
        Terminal.writeInputBuffer("move 1;0:2;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        prepareForMove(5);
        
        //move 3 
        Terminal.writeInputBuffer("move 1;0:2;0:3;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForMove(5);
        
      //move 4 
        Terminal.writeInputBuffer("move 1;0:2;0:3;0:4;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForMove(5);
        
      //move 5 
        Terminal.writeInputBuffer("move 1;0:2;0:3;0:4;0:5;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForMove(5);
        
      //move 6 
        Terminal.writeInputBuffer("move 1;0:2;0:3;0:4;0:5;0:6;0");
        Loop.loop(game);
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForMove(5);
        
      //move 7
        Terminal.writeInputBuffer("move 1;0:2;0:3;0:4;0:5;0:6;0:7;0");
        Loop.loop(game);
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForMove(5);
        
      //move 8
        Terminal.writeInputBuffer("move 1;0:2;0:3;0:4;0:5;0:6;0:7;0:8;0");
        Loop.loop(game);
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
    }
    
    
    @Test
    public void testMaxStep6() {
        //roll2----------------------------------------------------------------------
        prepareForMove(6);
        
        //move 0 fail
        Terminal.writeInputBuffer("move 0;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForMove(6);
        
        //move 1 ok
        Terminal.writeInputBuffer("move 1;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        prepareForMove(6);
        
        //move 2
        Terminal.writeInputBuffer("move 1;0:2;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        prepareForMove(6);
        
        //move 3 
        Terminal.writeInputBuffer("move 1;0:2;0:3;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForMove(6);
        
      //move 4 
        Terminal.writeInputBuffer("move 1;0:2;0:3;0:4;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));

        prepareForMove(6);
        
      //move 5 
        Terminal.writeInputBuffer("move 1;0:2;0:3;0:4;0:5;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForMove(6);
        
      //move 6 
        Terminal.writeInputBuffer("move 1;0:2;0:3;0:4;0:5;0:6;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForMove(6);
        
      //move 7
        Terminal.writeInputBuffer("move 1;0:2;0:3;0:4;0:5;0:6;0:7;0");
        Loop.loop(game);
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForMove(6);
        
      //move 8
        Terminal.writeInputBuffer("move 1;0:2;0:3;0:4;0:5;0:6;0:7;0:8;0");
        Loop.loop(game);
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
    }
    
    
    @Test
    public void testMaxStep7() {
        //roll2----------------------------------------------------------------------
        prepareForMove(7);
        
        //move 0 fail
        Terminal.writeInputBuffer("move 0;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("state 0;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
        
        prepareForMove(7);
        
        //move 1 ok
        Terminal.writeInputBuffer("move 1;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("state 1;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
        
        prepareForMove(7);
        
        //move 2
        Terminal.writeInputBuffer("move 1;0:2;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("state 2;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
        
        prepareForMove(7);
        
        //move 3 
        Terminal.writeInputBuffer("move 1;0:2;0:3;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("state 3;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
        
        prepareForMove(7);
        
      //move 4 
        Terminal.writeInputBuffer("move 1;0:2;0:3;0:4;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("state 4;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
        
        prepareForMove(7);
        
      //move 5 
        Terminal.writeInputBuffer("move 1;0:2;0:3;0:4;0:5;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("state 5;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
        
        prepareForMove(7);
        
      //move 6 
        Terminal.writeInputBuffer("move 1;0:2;0:3;0:4;0:5;0:6;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("state 6;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
        
        prepareForMove(7);
        
      //move 7
        Terminal.writeInputBuffer("move 1;0:2;0:3;0:4;0:5;0:6;0:7;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("state 7;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
        
        prepareForMove(7);
        
      //move 8
        Terminal.writeInputBuffer("move 1;0:2;0:3;0:4;0:5;0:6;0:7;0:8;0");
        Loop.loop(game);
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("state 0;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
    }

    private void prepareForMove2(int roll) {
        //places like
        //--++++...
        //------...
        //.........
        //.........V
        //..
        //Vesta in center
        
        String sRoll;
        if (roll == 7) {
            sRoll = "DAWN";
        } else {
            sRoll = Integer.toString(roll);
        }
        
        Terminal.writeInputBuffer("reset");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("set-vc 5;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll " + sRoll);
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 0;2:0;" + (roll + 1));
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));     
    }
    
    @Test
    public void testPathGap() {
        //1 gap
        prepareForMove2(7); //start 5;7
        Terminal.writeInputBuffer("move 5;8:5;10:5;11:5;12");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("state 5;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
        
      //1 gap 2x
        prepareForMove2(7); //start 5;7
        Terminal.writeInputBuffer("move 5;8:5;10:5;11:5;13;5:14");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("state 5;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
        
        //2 gap
        prepareForMove2(7); //start 5;7
        Terminal.writeInputBuffer("move 5;8:5;11:5;12");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("state 5;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
        
        //---------------------------------------------------------
        
      //1 gap
        prepareForMove2(7); //start 5;7
        Terminal.writeInputBuffer("move 7;7:8;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("state 5;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
        
      //1 gap 2x
        prepareForMove2(7); //start 5;7
        Terminal.writeInputBuffer("move 6;7:8;7:9;7:11;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("state 5;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
        
        //2 gap
        prepareForMove2(7); //start 5;7
        Terminal.writeInputBuffer("move 6;7:9;7:10;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("state 5;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
        
      //---------------------------------------------------------
        
        //1 gap
          prepareForMove2(7); //start 5;7
          Terminal.writeInputBuffer("move 5;5:5;4");
          Loop.loop(game);
          assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
          
          Terminal.writeInputBuffer("state 5;7");
          Loop.loop(game);
          assertTrue(Terminal.readOutputBuffer().equals("V"));
          
        //1 gap 2x
          prepareForMove2(7); //start 5;7
          Terminal.writeInputBuffer("move 5;5:5;4:5;2:5;1");
          Loop.loop(game);
          assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
          
          Terminal.writeInputBuffer("state 5;7");
          Loop.loop(game);
          assertTrue(Terminal.readOutputBuffer().equals("V"));
          
          //2 gap
          prepareForMove2(7); //start 5;7
          Terminal.writeInputBuffer("move 5;5:5;4");
          Loop.loop(game);
          assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
          
          Terminal.writeInputBuffer("state 5;7");
          Loop.loop(game);
          assertTrue(Terminal.readOutputBuffer().equals("V"));
          
        //---------------------------------------------------------
          
          //1 gap
            prepareForMove2(7); //start 5;7
            Terminal.writeInputBuffer("move 3;7");
            Loop.loop(game);
            assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
            
            Terminal.writeInputBuffer("state 5;7");
            Loop.loop(game);
            assertTrue(Terminal.readOutputBuffer().equals("V"));
            
          //1 gap 2x
            prepareForMove2(7); //start 5;7
            Terminal.writeInputBuffer("move 4;7:2;7:0;7");
            Loop.loop(game);
            assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
            
            Terminal.writeInputBuffer("state 5;7");
            Loop.loop(game);
            assertTrue(Terminal.readOutputBuffer().equals("V"));
            
            //2 gap
            prepareForMove2(7); //start 5;7
            Terminal.writeInputBuffer("move 4;7:2;7");
            Loop.loop(game);
            assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
            
            Terminal.writeInputBuffer("state 5;7");
            Loop.loop(game);
            assertTrue(Terminal.readOutputBuffer().equals("V"));
    }

    @Test
    public void testPathDiagonal() {
        //System.out.println("-----------------------------------------------------------------------------------------------");
        prepareForMove2(7); //5;7
        Terminal.writeInputBuffer("move 4;8:4;9");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("Error, Invalid path."));
        
        Terminal.writeInputBuffer("state 5;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
        
        prepareForMove2(7); //5;7
        Terminal.writeInputBuffer("move 5;8:4;9");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("Error, Invalid path."));
        
        Terminal.writeInputBuffer("state 5;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
        
        prepareForMove2(7); //5;7
        Terminal.writeInputBuffer("move 6;8:6;9");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("Error, Invalid path."));
        
        Terminal.writeInputBuffer("state 5;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
        
        prepareForMove2(7); //5;7
        Terminal.writeInputBuffer("move 6;7:7;8");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("Error, Invalid path."));
        
        Terminal.writeInputBuffer("state 5;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
        
        prepareForMove2(7); //5;7
        Terminal.writeInputBuffer("move 6;6:6;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("Error, Invalid path."));
        
        Terminal.writeInputBuffer("state 5;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
        
        prepareForMove2(7); //5;7
        Terminal.writeInputBuffer("move 5;6:6;5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("Error, Invalid path."));
        
        Terminal.writeInputBuffer("state 5;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
        
        prepareForMove2(7); //5;7
        Terminal.writeInputBuffer("move 4;6:3;6");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("Error, Invalid path."));
        
        prepareForMove2(7); //5;7
        Terminal.writeInputBuffer("move 4;7:3;6");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("Error, Invalid path."));
        
        Terminal.writeInputBuffer("state 5;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
    }
    
    @Test 
    public void testWalkOnTheSpot() {
        //auf der Stelle laufen
        prepareForMove2(7); //5;7
        Terminal.writeInputBuffer("move 5;8:5;8:5;9");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("Error, Invalid path."));
        
        Terminal.writeInputBuffer("state 5;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
        
        prepareForMove2(7); //5;7
        Terminal.writeInputBuffer("move 5;7:5;8");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("Error, Invalid path."));
        
        Terminal.writeInputBuffer("state 5;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
        
        prepareForMove2(7); //5;7
        Terminal.writeInputBuffer("move 6;7:6;7:7;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("Error, Invalid path."));
        
        Terminal.writeInputBuffer("state 5;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
        
        prepareForMove2(7); //5;7
        Terminal.writeInputBuffer("move 5;7:6;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("Error, Invalid path."));
        
        Terminal.writeInputBuffer("state 5;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
        
        prepareForMove2(7); //5;7
        Terminal.writeInputBuffer("move 5;6:5;6:5;5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("Error, Invalid path."));
        
        prepareForMove2(7); //5;7
        Terminal.writeInputBuffer("move 5;7:5;6");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("Error, Invalid path."));
        
        Terminal.writeInputBuffer("state 5;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
        
        prepareForMove2(7); //5;7
        Terminal.writeInputBuffer("move 4;7:4;7:3;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("Error, Invalid path."));
        
        prepareForMove2(7); //5;7
        Terminal.writeInputBuffer("move 5;7:4;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("Error, Invalid path."));
        
        Terminal.writeInputBuffer("state 5;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
    }

    @Test
    public void testStartIsEnd() {
        prepareForMove2(7); //5;7
        Terminal.writeInputBuffer("move 5;8:6;8:6;7:6;6:5;6:5;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("print");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals( "--+++++++------" + System.lineSeparator()
        + "---------------" + System.lineSeparator() 
        + "---------------" + System.lineSeparator() 
        + "---------------" + System.lineSeparator() 
        + "---------------" + System.lineSeparator() 
        + "-------V-------" + System.lineSeparator() 
        + "---------------" + System.lineSeparator() 
        + "---------------" + System.lineSeparator() 
        + "---------------" + System.lineSeparator() 
        + "---------------" + System.lineSeparator() 
        + "---------------"));
    }
    
    @Test
    public void testPathContainsStart() {
        prepareForMove2(7); //5;7
        Terminal.writeInputBuffer("move 5;8:4;8:4;7:5;7:6;7:7;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("state 7;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
    }
    
    @Test
    public void testWalkOutOfBoard() {
        prepareForMove(7); //0;0
        Terminal.writeInputBuffer("move 1;0:1;1:0;1:-1;1;0:1");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("Error, Not a valid command."));
        
        Terminal.writeInputBuffer("state 0;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
    }
    
    @Test
    public void testWayIsShut() {
        prepareForMove(7); //0;0
        Terminal.writeInputBuffer("move 0;1:0;2:0;3:0;4:0;5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("Error, Move not possible because the way is shut."));
    
        prepareForMove(7); //0;0
        Terminal.writeInputBuffer("move 1;0:1;1:1;2:0;2");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("Error, Move not possible because the way is shut."));
    
        Terminal.writeInputBuffer("state 0;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
    }
    
    @Test
    public void testWalkHV() {
        //horizontal and vertical steps ok
        prepareForMove2(7); //5;7
        Terminal.writeInputBuffer("move 5;8:4;8:4;9:5;9:5;10:4;10:4;11");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("state 4;11");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V"));
        
        Terminal.writeInputBuffer("state 5;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("-"));
    }
}
