package edu.kit.informatik.dawn;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.dawn.board.Game;

@RunWith(Parameterized.class)
public class TestChooseToken2SameRolls {
    Game game;
    
    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {     
                 { 2}, { 3}, { 4}, { 5}, { 6}, { 7}  
           });
    }
    
    @Parameter(0)
    public int roll;
    


    @Before
    public void setUp() throws Exception {
        game = new Game();
        Terminal.testMode = true;
        Terminal.printToConsoleInTestMode = false;
    }

    @After
    public void tearDown() throws Exception {
        Terminal.testMode = false;
        Terminal.printToConsoleInTestMode = false;
    }
    
    private void prepareForSecondPlace(int roll) {
        String sRoll;
        
        if (roll == 7) {
            sRoll = "DAWN";
        } else {
            sRoll = Integer.toString(roll);
        }

        
        Terminal.writeInputBuffer("reset");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("set-vc 10;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK")); 
        
        Terminal.writeInputBuffer("roll " + sRoll);
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 5;0:5;" + (roll - 1));
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 10;1");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll " + sRoll);
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
    }

    @Test
    public void test() {
        //normal: 4 auf feld, w�rfel 5 place 5, ok
        
        //4 auf feld, w�rfel 4, 
        //  place 3 ok
        //  place 5 ok
        //  place 4 n.ok
        //  place 2 n.ok
        //  place 6 n.ok
        //  place 7 n.ok
        //-> f�r alle
           
        prepareForSecondPlace(roll);
        Terminal.writeInputBuffer("place 0;1:0;2");
        Loop.loop(game);
        if (roll == 3) {
            assertTrue(Terminal.readOutputBuffer().equals("OK"));
        } else {
            assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        }
        
        System.out.println("--------------------------------------------------------------------------------");
        
        prepareForSecondPlace(roll);
        Terminal.writeInputBuffer("place 0;1:0;3");
        Loop.loop(game);
        if (roll == 4 || roll == 2) {
            assertTrue(Terminal.readOutputBuffer().equals("OK"));
        } else {
            assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        }
        
        System.out.println("--------------------------------------------------------------------------------");
        
        prepareForSecondPlace(roll);
        Terminal.writeInputBuffer("place 0;1:0;4");
        Loop.loop(game);
        if (roll == 5 || roll == 3) {
            assertTrue(Terminal.readOutputBuffer().equals("OK"));
        } else {
            assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        }
        
        System.out.println("--------------------------------------------------------------------------------");
        
        prepareForSecondPlace(roll);
        Terminal.writeInputBuffer("place 0;1:0;5");
        Loop.loop(game);
        if (roll == 6 || roll == 4) {
            assertTrue(Terminal.readOutputBuffer().equals("OK"));
        } else {
            assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        }
        
        System.out.println("--------------------------------------------------------------------------------");
        
        prepareForSecondPlace(roll);
        Terminal.writeInputBuffer("place 0;1:0;6");
        Loop.loop(game);
        if (roll == 7 || roll == 5) {
            assertTrue(Terminal.readOutputBuffer().equals("OK"));
        } else {
            assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        }
        
        System.out.println("--------------------------------------------------------------------------------");
        
        prepareForSecondPlace(roll);
        Terminal.writeInputBuffer("place 0;1:0;7");
        Loop.loop(game);
        if (roll == 6) {
            assertTrue(Terminal.readOutputBuffer().equals("OK"));
        } else {
            assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        }
        
        System.out.println("--------------------------------------------------------------------------------");
    }

    
    
//    private void test1OnField() {
//        prepareForSecondPlace(4, 4);
//        Terminal.writeInputBuffer("place 0;1:0;3");
//        Loop.loop(game);
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
//        
//        prepareForSecondPlace(4, 4);
//        Terminal.writeInputBuffer("place 0;1:0;5");
//        Loop.loop(game);
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
//        
//        prepareForSecondPlace(4, 4);
//        Terminal.writeInputBuffer("place 0;1:0;2");
//        Loop.loop(game);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
//        
//        prepareForSecondPlace(4, 4);
//        Terminal.writeInputBuffer("place 0;1:0;4");
//        Loop.loop(game);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
//        
//        prepareForSecondPlace(4, 4);
//        Terminal.writeInputBuffer("place 0;1:0;6");
//        Loop.loop(game);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
//        
//        prepareForSecondPlace(4, 4);
//        Terminal.writeInputBuffer("place 0;1:0;7");
//        Loop.loop(game);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
//    }

}
