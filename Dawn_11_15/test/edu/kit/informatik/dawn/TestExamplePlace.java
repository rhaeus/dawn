package edu.kit.informatik.dawn;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.dawn.board.Game;

public class TestExamplePlace {
    Game game;
    
    @Before
    public void setUp() throws Exception {
        game = new Game();
        Terminal.testMode = true;
    }

    @After
    public void tearDown() throws Exception {
        Terminal.testMode = false;
    }

    @Test
    public void test1() {
        //DAWN legen
        Terminal.writeInputBuffer("set-vc 0;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll DAWN");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 5;-6:5;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("print");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("V--------------" + System.lineSeparator()
                                                    + "---------------" + System.lineSeparator() 
                                                    + "---------------" + System.lineSeparator() 
                                                    + "---------------" + System.lineSeparator() 
                                                    + "---------------" + System.lineSeparator() 
                                                    + "+--------------" + System.lineSeparator() 
                                                    + "---------------" + System.lineSeparator() 
                                                    + "---------------" + System.lineSeparator() 
                                                    + "---------------" + System.lineSeparator() 
                                                    + "---------------" + System.lineSeparator() 
                                                    + "---------------"));
    }
    
    @Test
    public void test2() {
        //5 legen
        Terminal.writeInputBuffer("set-vc 0;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll 5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 0;-4:0;1");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
    }
    
    @Test
    public void test3() {
        //5 legen
        Terminal.writeInputBuffer("set-vc 1;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll 5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 0;0:0;4");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("print");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("+++++----------" + System.lineSeparator()
                                                    + "V--------------" + System.lineSeparator() 
                                                    + "---------------" + System.lineSeparator() 
                                                    + "---------------" + System.lineSeparator() 
                                                    + "---------------" + System.lineSeparator() 
                                                    + "---------------" + System.lineSeparator() 
                                                    + "---------------" + System.lineSeparator() 
                                                    + "---------------" + System.lineSeparator() 
                                                    + "---------------" + System.lineSeparator() 
                                                    + "---------------" + System.lineSeparator() 
                                                    + "---------------"));
    }
    
    @Test
    public void test4() {
        //5 legen
        Terminal.writeInputBuffer("set-vc 1;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll 5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 0;4:0;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("print");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("+++++----------" + System.lineSeparator()
                                                    + "V--------------" + System.lineSeparator() 
                                                    + "---------------" + System.lineSeparator() 
                                                    + "---------------" + System.lineSeparator() 
                                                    + "---------------" + System.lineSeparator() 
                                                    + "---------------" + System.lineSeparator() 
                                                    + "---------------" + System.lineSeparator() 
                                                    + "---------------" + System.lineSeparator() 
                                                    + "---------------" + System.lineSeparator() 
                                                    + "---------------" + System.lineSeparator() 
                                                    + "---------------"));
    }

}
